db.fruits.insertMany([
    {
        name: "Apple", 
        color: "Red", 
        stock: 20, 
        price: 40,
        supplier_id: 1, 
        onSale: true, 
        origin: ["Philippines", "US"]
    },
    {
        name: "Banana", 
        color: "Yellow", 
        stock: 15, 
        price: 20,
        supplier_id: 2, 
        onSale: true, 
        origin: ["Philippines", "Ecuador"]
    }, 
    {
        name: "Kiwi", 
        color: "Green", 
        stock: 25, 
        price: 50,
        supplier_id: 1, 
        onSale: true, 
        origin: ["US", "China"]
    }, 
    {
        name: "Mango", 
        color: "Yellow", 
        stock: 10, 
        price: 120,
        supplier_id: 2, 
        onSale: false, 
        origin: ["Philippines", "India"]
    }
]);

//mongoDB: aggregation 
/*
    mongoDB aggregation is used to generate manipulated and perform operation to create filterd results that helps in analyzing data.

    compared to doing CRUD operation, aggregationgives us access to manipulate filter and compute for results. Providing us with information to make necessary development without having to create a frontend application
*/

//using the aggregated method 
/*
    the "$match" is used to pass the documents taht meet the specified condition to the next pipeline state/ aggregation process

    syntax
        {$match: {field: value}}
        {$group: {_id "value", fieldResult: "valueResult"}}

    using both "$match" and "$group" alog wth aggregation will find for produicts that are on sale adn will group all stock for all suplier found 

        dn.collectionName.aggregate([
        {$match :{fieldA: valueA}},
        {$group{_id: "$fieldB"}, {result:{operation}}}
        ])


*/
db.fruits.aggregate([
{$match: {onSale: true}},
{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
]);


//field projection with aggregation
/*
    the $project can be used when aggregationg data to include / exclude field from the returned results 

    syntax 
        {$project: {field 1/0}}
*/

db.fruits.aggregate([
    {$match: {onSale:true}},
    {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
    {$project:{id_: 0}}
    ])

//sorting aggregated results
/*
    $sort can be used to change the order of aggregated results 

    providing a value of -1 will sort the aggregated results in a reverse order

    syntax
    {sort: {field: 1/-1}}
*/

db.fruits.aggregate([
    {$match: {onSale:true}},
    {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
    {$sort:{total: -1}}
])


//aggregating results based on array field 

/*
    the $unwind deconstruct an array field from a collection with an array to output a result for each element 

    the syantax below will return result creating seperate documents for each the countries provided per the origin field

    syntax
        {$unwind: "$field"}
*/
db.fruits.aggregate([
{$unwind: "$origin"}
])

//display fruits documanets by their origin and the kind of fruits that are supplied 

db.fruits.aggregate([
{$unwind: "$origin"},
{$group: {_id: "$supplier_id", kinds: {$sum:1}}}
])

db.fruits.aggregate([
    {$unwind: "$origin"},
    {$group: {_id: "$origin", kkinds: {$sum: 1}}}
])
























